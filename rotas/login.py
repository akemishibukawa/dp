from geral.config import *
from geral.cripto import *
from modelo.pessoa import Pessoa

@app.route("/")
def inicio():
    return 'Sistema de cadastro de pessoas. '+\
        '<a href="/incluir">Operação incluir pessoas</a>'

@app.route("/login", methods=['POST'])
def logar():
    # preparar uma resposta otimista
    resposta = jsonify({"resultado": "ok", "detalhes": "ok"})
    # receber as informações do novo objeto
    dados = request.get_json(force=True)  
    login = dados['email']
    senha = dados['senha']
    
    #verificar se login e senha estão corretos
    encontrado = Pessoa.query.filter_by(email=login, senha=cifrar(senha)).first
    if encontrado is not None:
        session[login]="OK"
    else:
        print("Erro de login")
        resposta = jsonify({"resultado": "Erro", "detalhes": "login e/ou senha inválido(s)"})        
    # adicionar cabeçalho de liberação de origem
    resposta.headers.add("Access-Control-Allow-Origin", "*")
    # permitir o envio dos cookies
    resposta.headers.add('Access-Control-Allow-Credentials', 'true')
    return resposta  # responder!


@app.route("/incluir_pessoa", methods=['POST'])
def incluir_pessoa():
    resposta = jsonify({"resultado": "ok", "detalhes": "ok"})
    dados = request.get_json()
    print((dados))
    try:
        if dados['email'].strip() == '' or dados['senha'].strip() == '' or dados['nome'].strip() == '':
            raise Exception("Não deixe nenhum campo vazio!")
        nova = Pessoa(**dados)
        db.session.add(nova)
        db.session.commit() 
    except Exception as e: 
      resposta = jsonify({"resultado":"Erro", "detalhes":str(e)})
    resposta.headers.add("Access-Control-Allow-Origin", "*")
    return resposta 

app.run(debug=True)
